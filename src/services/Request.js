import axios from 'axios';

const DEFAULT_URL = 'https://api.themoviedb.org/3';

export default class Request {
    // Send http requests
    static async get(url) {
        try {
            const response = await axios(
                `${DEFAULT_URL}${url}&api_key=d5ab1c88c6fa0649cd46d8723ada06a0`,
                {
                    method: 'GET',
                },
            );
            return response.data;
        } catch (e) {
            console.log(e);
        }
    }
}
