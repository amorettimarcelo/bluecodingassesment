import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './screens/Home';
import AllMovies from './screens/AllMovies';
import MovieDetails from './screens/MovieDetails';

const stackNavigator = createStackNavigator({
    Home: {
        screen: Home,
    },
    MovieDetails: {
        screen: MovieDetails,
    },
    AllMovies: {
        screen: AllMovies,
    },
});

export default createAppContainer(stackNavigator);
