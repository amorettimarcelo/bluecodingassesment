import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Styles from '../styles/TitleStyles';

class Title extends Component {
    render() {
        const { extraBold, semiBold, navigate } = this.props;
        return (
            <View style={Styles.recommendedBox}>
                <View style={Styles.recommendedTextRow}>
                    <Text style={Styles.recommend}>{extraBold} </Text>
                    <Text style={Styles.recommendFor}>{semiBold}</Text>
                </View>
                <TouchableOpacity style={Styles.showAll} onPress={navigate}>
                    <Text style={Styles.showAllText}>Show All</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default Title;
