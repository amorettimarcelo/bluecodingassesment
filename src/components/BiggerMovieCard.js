import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';
import Styles from '../styles/BiggerMovieStyles';

class BiggerMovieCard extends Component {
    render() {
        const { image, id } = this.props;
        return (
            <TouchableOpacity
                onPress={() =>
                    this.props.navigation.navigate('MovieDetails', {
                        url: `/movie/${id}?language=en-US`,
                    })
                }>
                <Image
                    style={Styles.image}
                    source={{
                        uri: `https://image.tmdb.org/t/p/w92/${image}`,
                    }}
                />
            </TouchableOpacity>
        );
    }
}
export default withNavigation(BiggerMovieCard);
