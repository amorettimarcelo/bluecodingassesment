import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import Styles from '../styles/HeaderStyles';

class Header extends Component {
    render() {
        return (
            <View>
                <View style={Styles.headerBox}>
                    <View style={Styles.titleBox}>
                        <Text style={Styles.title}>THE MOVIES</Text>
                    </View>
                    <Image
                        style={Styles.myavatarImage}
                        source={require('../assets/images/myAvatar.png')}
                        resizeMode="contain"
                    />
                </View>
            </View>
        );
    }
}

export default Header;
