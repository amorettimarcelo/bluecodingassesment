import React, { Component } from 'react';
import { View, FlatList, Text, ActivityIndicator } from 'react-native';
import MovieCard from './MovieCard';
import Styles from '../styles/MovieCarrousselStyle';
import Request from '../services/Request';

class MovieCarroussel extends Component {
    constructor() {
        super();

        this.state = {
            movies: [],
            loading: true,
        };
    }

    componentDidMount() {
        Request.get(this.props.url).then(response =>
            this.setState({ movies: response.results, loading: false }),
        );
    }
    render() {
        return !this.state.loading ? (
            <FlatList
                horizontal={true}
                data={this.state.movies}
                initialNumToRender={10}
                renderItem={data => this.renderItem(data.item)}
                ListEmptyComponent={() => this.renderEmpty()}
                keyExtractor={item => item.id.toString()}
                style={Styles.FlatListStyle}
            />
        ) : (
            <ActivityIndicator size="large" />
        );
    }

    renderItem(data) {
        return <MovieCard image={data.poster_path} id={data.id} />;
    }

    renderEmpty = () => {
        return (
            <View>
                <Text>Empty</Text>
            </View>
        );
    };
}

export default MovieCarroussel;
