import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import Styles from '../styles/CastComponentStyles';

export default class CastComponent extends Component {
    render() {
        const { name, image } = this.props;
        return (
            <View style={Styles.container}>
                <Image
                    style={Styles.imageContainer}
                    source={{
                        uri: `https://image.tmdb.org/t/p/w154/${image}`,
                    }}
                />
                <Text style={Styles.textContainer} numberOfLines={1}>
                    {name}
                </Text>
            </View>
        );
    }
}
