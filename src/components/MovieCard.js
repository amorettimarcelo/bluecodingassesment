import React, { Component } from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { withNavigation } from 'react-navigation';
import Styles from '../styles/MovieCardStyles';

class MovieCard extends Component {
    render() {
        const { id, image } = this.props;
        return (
            <TouchableOpacity
                onPress={() =>
                    this.props.navigation.navigate('MovieDetails', {
                        url: `/movie/${id}?language=en-US`,
                    })
                }>
                <Image
                    style={Styles.movieImage}
                    source={{
                        uri: `https://image.tmdb.org/t/p/w92/${image}`,
                    }}
                />
            </TouchableOpacity>
        );
    }
}

export default withNavigation(MovieCard);
