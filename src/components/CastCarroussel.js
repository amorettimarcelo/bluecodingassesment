import React, { Component } from 'react';
import { View, FlatList, Text, ActivityIndicator } from 'react-native';
import CastComponent from './CastComponent';
import Styles from '../styles/MovieCarrousselStyle';
import Request from '../services/Request';

class CastCarroussel extends Component {
    constructor() {
        super();

        this.state = {
            cast: [],
            loading: true,
        };
    }

    componentDidMount() {
        Request.get(`/movie/${this.props.id}/credits?language=en-US`).then(
            response => this.setState({ cast: response.cast, loading: false }),
        );
    }
    render() {
        return !this.state.loading ? (
            <FlatList
                horizontal={true}
                data={this.state.cast}
                initialNumToRender={10}
                renderItem={data => this.renderItem(data.item)}
                ListEmptyComponent={() => this.renderEmpty()}
                keyExtractor={item => item.id.toString()}
                style={Styles.FlatListStyle}
            />
        ) : (
            <ActivityIndicator size="large" />
        );
    }

    renderItem(data) {
        return <CastComponent image={data.profile_path} name={data.name} />;
    }

    renderEmpty = () => {
        return (
            <View>
                <Text>The component is empty</Text>
            </View>
        );
    };
}

export default CastCarroussel;
