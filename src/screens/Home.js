import React, { Component } from 'react';
import { SafeAreaView, ScrollView, StyleSheet } from 'react-native';
import Header from '../components/Header';
import MovieCarroussel from '../components/MovieCarroussel';
import Title from '../components/Title';

class Home extends Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <SafeAreaView style={Styles.mainContainer}>
                <ScrollView>
                    <Header />

                    <Title
                        extraBold="Recommended"
                        semiBold="For You"
                        navigate={() => {
                            this.props.navigation.navigate('AllMovies', {
                                url:
                                    '/discover/movie?with_genres=18&sort_by=vote_average.desc&vote_count.gte=20',
                                title: 'Recommended For You',
                            });
                        }}
                    />
                    <MovieCarroussel url="/discover/movie?with_genres=18&sort_by=vote_average.desc&vote_count.gte=20" />

                    <Title
                        extraBold="Popular"
                        semiBold="Movies"
                        navigate={() => {
                            this.props.navigation.navigate('AllMovies', {
                                url: '/discover/movie?sort_by=popularity.desc',
                                title: 'Popular Movies',
                            });
                        }}
                    />
                    <MovieCarroussel url="/discover/movie?sort_by=popularity.desc" />

                    <Title
                        extraBold="Comming"
                        semiBold="Soon"
                        navigate={() => {
                            this.props.navigation.navigate('AllMovies', {
                                url: '/movie/upcoming?language=en-US',
                                title: 'Comming Soon',
                            });
                        }}
                    />
                    <MovieCarroussel url="/movie/upcoming?language=en-US" />

                    <Title
                        extraBold="Top Rated"
                        semiBold="Movies"
                        navigate={() => {
                            this.props.navigation.navigate('AllMovies', {
                                url: '/movie/top_rated?language=en-US',
                                title: 'Top Rated Movies',
                            });
                        }}
                    />
                    <MovieCarroussel url="/movie/top_rated?language=en-US" />
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const Styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
});
export default Home;
