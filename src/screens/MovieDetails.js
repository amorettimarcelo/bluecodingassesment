import React, { Component } from 'react';
import {
    View,
    Image,
    ImageBackground,
    ScrollView,
    Text,
    ActivityIndicator,
} from 'react-native';
import Styles from '../styles/MovieDetailsStyles';
import Request from '../services/Request';
import CastCarroussel from '../components/CastCarroussel';
import MovieCarroussel from '../components/MovieCarroussel';

export default class MovieDetails extends Component {
    constructor() {
        super();

        this.state = {
            movie: {},
            loading: true,
        };
    }
    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        let url = this.props.navigation.getParam('url');
        if (url) {
            Request.get(url).then(response =>
                this.setState({ movie: response, loading: false }),
            );
        }
    }

    render() {
        const { movie } = this.state;
        console.log(movie);
        return !this.state.loading ? (
            <ScrollView>
                <ImageBackground
                    style={Styles.bgImageStyles}
                    source={{
                        uri: `http://image.tmdb.org/t/p/w300/${
                            movie.backdrop_path
                        }`,
                    }}>
                    <Text style={Styles.movieTitle}>
                        {movie.original_title}
                    </Text>
                    <View style={Styles.rating}>
                        <Image
                            style={Styles.iconStyle}
                            source={require('../assets/images/star.png')}
                        />
                        <Text style={Styles.ratingText}>
                            {movie.vote_average} ({movie.vote_count} Reviews)
                        </Text>
                    </View>
                    <View style={Styles.duration}>
                        <Image
                            style={Styles.iconStyle}
                            source={require('../assets/images/clock.png')}
                        />
                        <Text style={Styles.durationText}>
                            {movie.runtime} mins
                        </Text>
                    </View>
                    <View style={Styles.released}>
                        <Image
                            style={Styles.iconStyle}
                            source={require('../assets/images/calendar.png')}
                        />
                        <Text style={Styles.releasedText}>
                            {movie.release_date} Released
                        </Text>
                    </View>
                </ImageBackground>
                <View style={Styles.grayView}>
                    <View style={Styles.bookmarkView}>
                        <Image
                            style={Styles.bookmarkIcon}
                            source={require('../assets/images/bookmark.png')}
                        />
                        <Text style={Styles.bookmarkText}>Watchlist</Text>
                    </View>
                    <View style={Styles.favouriteView}>
                        <Image
                            style={Styles.favouriteIcon}
                            source={require('../assets/images/heart.png')}
                        />
                        <Text style={Styles.favouriteText}>Favourite</Text>
                    </View>
                    <View style={Styles.shareView}>
                        <Image
                            style={Styles.shareIcon}
                            source={require('../assets/images/share.png')}
                        />
                        <Text style={Styles.shareText}>Share</Text>
                    </View>
                </View>
                <Image
                    style={Styles.profileImage}
                    source={{
                        uri: `http://image.tmdb.org/t/p/w92/${
                            movie.poster_path
                        }`,
                    }}
                />
                <View style={Styles.overviewBox}>
                    <Text style={Styles.overviewHeader}>Overview</Text>
                    <Text style={Styles.overviewText}>{movie.overview}</Text>
                    <Text style={Styles.readMore}>Read More</Text>
                </View>

                <View style={Styles.castHeader}>
                    <Text style={Styles.castTitle}>Cast</Text>
                    <Text style={Styles.castViewAll}>view all</Text>
                </View>

                <View>
                    <CastCarroussel id={movie.id} />
                </View>

                <View style={Styles.recommendationBox}>
                    <Text style={Styles.recommendationText}>Recomendation</Text>
                    <MovieCarroussel
                        url={`/movie/${
                            movie.id
                        }/recommendations?language=en-US`}
                    />
                </View>
            </ScrollView>
        ) : (
            <ActivityIndicator size="large" />
        );
    }
}
