import React, { Component } from 'react';
import { View, FlatList, ActivityIndicator, Text } from 'react-native';
import { moderateScale } from 'react-native-size-matters';
import Request from '../services/Request';
import BiggerMovieCard from '../components/BiggerMovieCard';

class AllMovies extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerBackTitle: null,
            headerTitle: navigation.getParam('title'),
            headerTintColor: '#e53627',
            headerTitleStyle: {
                fontSize: moderateScale(18),
                fontWeight: '500',
                letterSpacing: -0.43,
                lineHeight: moderateScale(22),
                fontFamily: 'Montserrat-Regular',
            },
        };
    };

    constructor() {
        super();

        this.state = {
            movies: [],
            loading: true,
        };
    }

    componentDidMount() {
        let url = this.props.navigation.getParam('url', null);
        if (url) {
            Request.get(url).then(response =>
                this.setState({ movies: response.results, loading: false }),
            );
        }
    }
    render() {
        return !this.state.loading ? (
            <FlatList
                data={this.state.movies}
                initialNumToRender={10}
                renderItem={data => this.renderItem(data.item)}
                ListEmptyComponent={() => this.renderEmpty()}
                keyExtractor={item => item.id.toString()}
                numColumns={2}
            />
        ) : (
            <ActivityIndicator size="large" />
        );
    }

    renderItem(data) {
        return <BiggerMovieCard image={data.poster_path} id={data.id} />;
    }

    renderEmpty = () => {
        return (
            <View>
                <Text>Component is empty</Text>
            </View>
        );
    };
}

export default AllMovies;
