import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    FlatListStyle: {
        paddingHorizontal: moderateScale(10),
    },
});
