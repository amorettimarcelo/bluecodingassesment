import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    bgImageStyles: {
        width: moderateScale(375),
        height: moderateScale(253),
        resizeMode: 'contain',
    },

    movieTitle: {
        marginTop: moderateScale(89),
        color: '#ffffff',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(20),
        fontWeight: '600',
        letterSpacing: -moderateScale(0.48),
        lineHeight: moderateScale(22),
        alignSelf: 'center',
    },

    genres: {
        marginTop: moderateScale(1),
        color: '#ffffff',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(13),
        fontWeight: '400',
        alignSelf: 'center',
    },

    profileImage: {
        position: 'absolute',
        width: moderateScale(120),
        height: moderateScale(180),
        borderRadius: moderateScale(8),
        borderColor: '#969696',
        borderWidth: moderateScale(1),
        marginTop: moderateScale(164),
        left: moderateScale(26),
    },

    rating: {
        position: 'absolute',
        top: moderateScale(181),
        left: moderateScale(162),
        flexDirection: 'row',
        justifyContent: 'center',
    },

    ratingText: {
        width: moderateScale(111),
        height: moderateScale(17),
        color: '#ffffff',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(13),
        fontWeight: '400',
        paddingHorizontal: moderateScale(7),
        alignSelf: 'center',
    },

    duration: {
        position: 'absolute',
        top: moderateScale(205),
        left: moderateScale(162),
        flexDirection: 'row',
        justifyContent: 'center',
    },

    durationText: {
        color: '#ffffff',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(13),
        fontWeight: '400',
        paddingHorizontal: moderateScale(7),
        alignSelf: 'center',
    },

    released: {
        position: 'absolute',
        top: moderateScale(229),
        left: moderateScale(162),
        flexDirection: 'row',
        justifyContent: 'center',
    },

    releasedText: {
        color: '#ffffff',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(13),
        fontWeight: '400',
        paddingHorizontal: moderateScale(7),
        alignSelf: 'center',
    },

    iconStyle: {
        width: moderateScale(14),
        height: moderateScale(14),
    },

    grayView: {
        height: moderateScale(104),
        backgroundColor: '#f4f4f4',
        flexDirection: 'row',
    },

    bookmarkView: {
        position: 'absolute',
        left: moderateScale(162),
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: moderateScale(21),
    },

    bookmarkIcon: {
        width: moderateScale(18),
        height: moderateScale(18),
        resizeMode: 'contain',
    },

    bookmarkText: {
        color: '#656565',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(12),
        fontWeight: '400',
    },

    favouriteView: {
        position: 'absolute',
        left: moderateScale(238),
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: moderateScale(21),
    },

    favouriteIcon: {
        width: moderateScale(18),
        height: moderateScale(18),
        resizeMode: 'contain',
    },

    favouriteText: {
        color: '#656565',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(12),
        fontWeight: '400',
    },

    shareView: {
        position: 'absolute',
        left: moderateScale(317),
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: moderateScale(21),
    },

    shareIcon: {
        width: moderateScale(18),
        height: moderateScale(18),
        resizeMode: 'contain',
    },

    shareText: {
        color: '#656565',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(12),
        fontWeight: '400',
    },

    overviewBox: {
        paddingLeft: moderateScale(26),
        paddingTop: moderateScale(12),
        paddingRight: moderateScale(24),
    },

    overviewHeader: {
        color: '#3b3b3b',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(18),
        fontWeight: '600',
        letterSpacing: -moderateScale(0.43),
        lineHeight: moderateScale(22),
    },

    overviewText: {
        color: '#505050',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(16),
        fontWeight: '400',
    },

    readMore: {
        color: '#5252f7',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(12),
        fontWeight: '500',
        letterSpacing: -moderateScale(0.29),
        lineHeight: moderateScale(22),
    },

    castHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: moderateScale(26),
        paddingRight: moderateScale(25),
        paddingTop: moderateScale(19),
        paddingBottom: moderateScale(7),
    },

    castTitle: {
        color: '#3b3b3b',
        fontFamily: 'Montserrat-Regular',
        fontSize: 18,
        fontWeight: '600',
        letterSpacing: -0.43,
        lineHeight: 22,
    },

    castViewAll: {
        color: '#5252f7',
        fontFamily: 'Montserrat',
        fontSize: moderateScale(12),
        fontWeight: '500',
        letterSpacing: -moderateScale(0.29),
        lineHeight: moderateScale(22),
    },

    recommendationBox: {
        paddingVertical: 20,
    },
    recommendationText: {
        color: '#3b3b3b',
        fontFamily: 'Montserrat',
        fontSize: moderateScale(18),
        fontWeight: '600',
        letterSpacing: -moderateScale(0.43),
        lineHeight: moderateScale(22),
        paddingVertical: moderateScale(8),
        paddingHorizontal: moderateScale(18),
    },
});
