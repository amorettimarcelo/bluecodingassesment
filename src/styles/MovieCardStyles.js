import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    movieImage: {
        height: moderateScale(150),
        width: moderateScale(100),
        borderRadius: moderateScale(8),
        margin: moderateScale(6),
    },
});
