import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    image: {
        width: moderateScale(160),
        height: moderateScale(226),
        borderRadius: moderateScale(10),
        margin: moderateScale(9),
    },
});
