import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

const width = Dimensions.get('window').width;
export default StyleSheet.create({
    headerBox: {
        flexDirection: 'row',
        paddingTop: moderateScale(52),
        width: width,
    },

    titleBox: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: moderateScale(30),
    },

    title: {
        color: '#e53627',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(18),
        fontWeight: '700',
        letterSpacing: -0.43,
        lineHeight: moderateScale(22),
    },

    myavatarImage: {
        width: moderateScale(28),
        height: moderateScale(28),
        borderRadius: moderateScale(14),
        marginRight: moderateScale(16),
        alignSelf: 'flex-end',
        borderColor: '#707070',
    },
});
