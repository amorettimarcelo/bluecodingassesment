import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    recommendedBox: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: moderateScale(27),
        width: '100%',
    },

    recommendedTextRow: {
        flexDirection: 'row',
        paddingHorizontal: moderateScale(16),
    },

    recommendFor: {
        color: '#3e3e3e',
        fontFamily: 'Montserrat-SemiBold',
        fontSize: moderateScale(18),
        fontWeight: '400',
        letterSpacing: -moderateScale(0.43),
        lineHeight: moderateScale(22),
    },

    recommend: {
        color: '#3b3b3b',
        fontFamily: 'Montserrat-ExtraBold',
        fontWeight: '600',
        fontSize: moderateScale(18),
    },

    showAll: {
        paddingRight: moderateScale(15),
        alignContent: 'flex-end',
        alignSelf: 'flex-end',
        justifyContent: 'flex-end',
    },

    showAllText: {
        color: '#5252f7',
        fontFamily: 'Montserrat-Regular',
        fontSize: moderateScale(14),
        fontWeight: '500',
        letterSpacing: -moderateScale(0.34),
        lineHeight: moderateScale(22),
        alignSelf: 'flex-end',
    },
});
