import { StyleSheet, Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

export default StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
    },

    imageContainer: {
        width: moderateScale(60),
        height: moderateScale(60),
        borderRadius: moderateScale(60),
        resizeMode: 'contain',
    },

    textContainer: {
        paddingTop: moderateScale(4),
        width: moderateScale(68),
    },
});
