# Instalation

    * Checkout Repository
    ```yarn install ```

## Running the project

    ```yarn start --reset-cache````
    * for iOS:
    simply run the command ```yarn ios````

    * for Android:
    Open your preffered emulator and run ```yarn android```

## About the project

    The project was developed just by checking and comparing the current code with the design provided by the recruiter..
